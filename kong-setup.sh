    # Task Service
curl -i -X POST \
	--url http://localhost:8001/services/ \
	--data 'name=task-service' \
	--data 'url=http://110.74.194.123:6072'
curl -i -X POST \
	--url http://localhost:8001/services/task-service/routes \
	--data 'name=task-api' \
	--data 'paths[]=/' \
	--data 'strip_path=false'

    # User Service
curl -i -X POST \
	--url http://localhost:8001/services/ \
	--data 'name=user-service' \
	--data 'url=http://110.74.194.123:6070/'
curl -i -X POST \
	--url http://localhost:8001/services/user-service/routes \
	--data 'name=user-api' \
	--data 'paths[]=/' \
	--data 'strip_path=false'

# $ chmod 744 script.sh 
#now the file is ready to run run it with $ ./<name of scirpt file e.g. script.sh>
# $ ./script.sh